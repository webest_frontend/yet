const webpack = require('webpack');
module.exports = {
        mode: 'development',
        entry: './src/index.js',
        output: {
            filename: 'app.js',
            path: __dirname + '/dist/js'
        },
        devtool: 'source-map',
        watch: true,
        optimization: {
            splitChunks: {
                chunks: 'all',
                name: 'vendor'
            }
        },
        plugins: [
            new webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery'
            })
        ]
};
import $ from 'jquery';
window.$ = window.jQuery = $;
import 'magnific-popup';
import 'jquery-mask-plugin';
import 'slick-carousel';
import './js/bootstrap';
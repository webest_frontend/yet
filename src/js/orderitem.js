export class OrderItem {
    constructor (container) {
        this.container = container;
        this.form = this.container.closest('form');
        this.removeBtn = this.container.querySelector('.order__remove');
    }

    init () {
        this.removeBtn.addEventListener('click', this._onRemove.bind(this));
        return this;
    }

    _onRemove (e) {
        var target = window[e.target.htmlFor];
        target.checked = !target.checked;
        target.dispatchEvent(new Event('change', {bubbles: true}));
        e.preventDefault();
        var id = this.container.closest('[data-product]').dataset.product;
        $(this.form).removeClass('with-' + id);
    }
}

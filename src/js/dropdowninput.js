export class DropDownInput {

    constructor (container) {
        this.container = container;
        this.inputs = this.container.querySelectorAll('input');
        this.selected = this.container.querySelector('.wrap__dropdown_selected');
    }

    init () {
        this.container.addEventListener('click', this._onClick.bind(this));
        return this;
    }

    _onClick (e) {
        if(e.target instanceof HTMLInputElement) return;
        e.stopPropagation();
        this.container.classList.toggle('expanded');
        var label = e.target.closest('label');
        if (label) {
            this.selected.innerText = label.querySelector('span').innerText;
        }
    }

    close() {
        this.container.classList.remove('expanded');
    }

    reset () {
        this.inputs.forEach(i => {
            i.checked = false;
        });
        this.inputs.item(0).checked = true;
        this.selected.innerText = this.inputs.item(0).value;
    }
}
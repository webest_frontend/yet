export class SpinnerInput {

    constructor (container) {
        this.container = container;
        this.input = this.container.querySelector('input');
        this.plus = this.container.querySelector('.plus');
        this.minus = this.container.querySelector('.minus');
        this.display = this.container.querySelector('.display');
    }

    init () {
        this.container.addEventListener('click', this._onClick.bind(this));
        return this;
    }

    _onClick (e) {
        if (e.target.classList.contains('plus')) {
            this.input.value = Math.min(parseInt(this.input.value) + 1, this.input.max);
        } else if (e.target.classList.contains('minus')) {
            this.input.value = Math.max(parseInt(this.input.value) - 1, 1);
        }
        this.display.innerText = this.input.value;
        this.input.dispatchEvent(new Event('change', {bubbles: true}));
    }

    reset () {
        $(this.container).removeClass('valid invalid');
    }
}
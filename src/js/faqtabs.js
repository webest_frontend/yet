import { Accordion } from './accordion';

export class FAQTabs {
    constructor (container) {
        this.container = container;
        this.nav = this.container.querySelector('.faq__nav');
        this.panels = this.container.querySelectorAll('.faq__panel');
        this.accordion = this.container.querySelectorAll('.accordion');
        this.activeTab = this.panels.item(0);
    }

    init () {
        $(this.activeTab).show();
        this.accordion.forEach(a => {
            new Accordion (a).init();
        });
        this.nav.addEventListener('click', this._onTabActivate.bind(this));
        return this;
    }

    _onTabActivate (e) {
        if (e.target.dataset.id) {
            $(e.target)
                .addClass('active')
                .siblings('li')
                .removeClass('active');
            const intendedToActivate = $(this.panels)
                .filter(function () {
                    return this.id === e.target.dataset.id;
                })
                .eq(0);
            $(this.activeTab).fadeOut(() => $(intendedToActivate).fadeIn());
            this.activeTab = intendedToActivate;
        }
    }
}
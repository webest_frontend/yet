export class Accordion {
    constructor (container) {
        this.container = container;
    }

    init () {
        this.container.addEventListener('click', this._onClick.bind(this));
        return this;
    }

    _onClick (e) {
        const target = $(e.target).closest('.accordion__set');
        if (target.length) {
            $(target)
                .toggleClass('expanded')
                .children('.accordion__content')
                .finish()
                .slideToggle(200);
        }
    }
}
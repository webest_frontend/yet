export class App {

    constructor (container, components) {
        this.container = container;
        this.components = components;
        this.preloader = document.querySelector('#preloader');
        this.navbarOffcetHeight = 124;
        this.scrolledDown = false;
        this.modalConfig = {
            closeBtnInside: true,
            mainClass: 'mfp-fade'
        };
    }

    init () {
        var self = this;
    	for (let i in this.components) {
    		if (Array.isArray(this.components[i])) {
                this.components[i].forEach(c => c.init(self));
            } else {
                this.components[i].init(this);
            }
    	}
        this.container.addEventListener('click', this._onClick.bind(this));
        window.addEventListener('scroll', this._onScroll.bind(this));
        document.querySelectorAll('.modal-trigger').forEach(e => {
            $(e).magnificPopup(self.modalConfig);
        });
        return this;

    }

    _onClick (e) {
        var exception, parent;
        parent = $(e.target).closest('.dropdown').get(0);
        exception = $(parent).children('.dropdown__list');
        if (parent && parent.dataset && parent.dataset.menu) {
            exception = $(exception).add('#' + parent.dataset.menu);
        }
        if (e.target.dataset && e.target.dataset.section) {
            scrollToSection.call(e.target);
        }
        $('.dropdown__list, .overlay_content')
            .not(exception)
            .finish()
            .slideUp(200);
        try {
            this.components.adapterform.closeAllDropDowns();
        } catch (e) {}

        function scrollToSection () {
            const section = this.dataset.section;
            try {
                document
                    .getElementById(section)
                    .scrollIntoView({behavior: 'smooth'});
            } catch (e) {
                location.assign('/index.php#' + section);
            }
        }
    }

    _onScroll (e) {
        if(this.scrolledDown) {
            if(window.scrollY <= this.navbarOffcetHeight) {
                document.body.classList.remove('sticked_header');
                this.scrolledDown = false;
            }
        } else if(window.scrollY > this.navbarOffcetHeight) {
            document.body.classList.add('sticked_header');
            this.scrolledDown = true;
        }
    }

    startLoader () {
        $(this.preloader).show();
    }

    stopLoader () {
        $(this.preloader).hide();
    }
}
export class CatalogSlider {
    constructor(container) {
        this.slider = container;
        this.slider_config = {
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            waitForAnimate: false,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 5000,
            pauseOnFocus: false,
            pauseOnHover: false
        };
    }

    init() {
        $(this.slider).slick(this.slider_config);
        return this;
    }
}
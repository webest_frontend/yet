export class TextInput {

    constructor (container) {
        this.container = container;
        this.input = this.container.querySelector('input');
        this.placeholder = this.container.querySelector('.placeholder');
        this.errorString = this.container.querySelector('.error-string');
    }

    init () {
        this.input.addEventListener('focus', this._onFocus.bind(this));
        this.input.addEventListener('blur', this._onBlur.bind(this));
        return this;
    }

    _onBlur () {
        var error, addClass;
        error = '';
        addClass = this.input.value ? 'valid' : '';
        $(this.placeholder).toggleClass('shifted', this.input.value);
        if (!this.input.checkValidity()) {
            error = this.input.validationMessage;
            addClass = 'invalid';
        }
        $(this.container)
            .removeClass('invalid valid')
            .addClass(addClass)
        $(this.errorString).text(error);
    }

    _onFocus () {
        $(this.container).removeClass('invalid valid');
        $(this.placeholder).toggleClass('shifted', true);
    }

    reset () {
        $(this.container).removeClass('valid invalid');
    }
}
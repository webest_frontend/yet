import { App } from './app';
import { CustomForm } from './customform';
import { FAQTabs } from './faqtabs';
import { DropDownMenu } from './dropdownmenu';
import { CatalogSlider } from './catalog_slider';
import(/* webpackChunkName: "adapter_form" */ './adapterform').then(chunk => {
	//console.log(chunk);
	document.querySelectorAll('#order-form').forEach(e => {
		app.components.adapterform = new chunk.AdapterForm (e).init(app);
	});
});
var app;
$(function () {
	var components = {
		dropdowns: [],
		orderitem: [],
		sliders: []
	};
	document.querySelectorAll('#buying_adapter_form').forEach(e => {
		components.customform = new CustomForm (e);
	});
	document.querySelectorAll('.faq__tabs').forEach(e => {
		components.faqtabs = new FAQTabs (e);
	});
	document.querySelectorAll('.dropdown').forEach(e => {
		components.dropdowns.push(new DropDownMenu (e));
	});
	document.querySelectorAll('.catalog__slider').forEach(e => {
		components.sliders.push(new CatalogSlider (e));
	});
	app = new App (document.body, components).init();
});


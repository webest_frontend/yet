import { TextInput } from './textinput';

export class CustomForm {
    constructor (form) {
        this.form = form;
        this.inputs = this.form.querySelectorAll(".wrap__text");
        this.maskedInputs = this.form.querySelectorAll('.wrap__mask');
        this.submitBtn = this.form.querySelector("[type='submit']");
        this.modal = this.form.dataset.modal;
        this.modalConfig = {
            items: {
                src: $('#' + this.modal),
                type: 'inline'
            },
            mainClass: 'mfp-fade',
            showCloseBtn: false
        };
    }

    init (app=null) {
        this.app = app;
        this.form.addEventListener('submit', this._onSubmit.bind(this));
        this.inputs = Array.from(this.inputs).map(i => new TextInput (i).init());
        $(this.maskedInputs)
            .find('input')
            .mask("+0 (000)-000-00-00", {placeholder: "+ (___)-___-__-__"});
        return this;
    }

    _onSubmit (e) {
        const self = this;
        var responseText;
        e.preventDefault();
        this.submitBtn.disabled = true;
        this.app.startLoader();
        /*$.ajax({
            url: this.form.action,
            method: 'POST',
            dataType: 'json',
            contentType: 'application/x-www-form-urlencoded',
            data: $(this.form).serialize(),
            success: this._onSuccess.bind(this),
            error: this._onError.bind(this),
            complete: this._onComplete.bind(this)
        });*/
        responseText = "<div class='heading'>" +
                "Payment successfully took place" +
                "</div>" +
                "<p>" +
                    "Your order is taken! Thank you for choosing us." +
                "</p>";
        new Promise(resolve => {
            setTimeout(resolve.bind({}, {html: responseText}), 2000);
        })
        .then(res => {
            self._onSuccess.call(this, res);
        })
        .then(() => {
            self._onComplete.call(this);
        });
    }

    reset () {
        Array.from(this.inputs).forEach(i => i.reset());
        this.form.reset();
    }

    _onSuccess (response) {
        this.reset.call(this);
        $(this.modal)
            .toggleClass('error', false)
            .find('.dummy-container')
            .html(response.html);
    }

    _onError (XHR) {
        $(this.modal)
            .toggleClass('error', true)
            .find('.dummy-container')
            .text(XHR.responseJSON.message || XHR.responseText);
    }

    _onComplete() {
        $.magnificPopup.open(this.modalConfig);
        this.submitBtn.disabled = false;
        this.app.stopLoader();
    }
}
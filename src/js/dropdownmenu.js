export class DropDownMenu {
	constructor (container) {
		this.container = container;
	}

	init () {
		this.container.addEventListener('click', this._onClick.bind(this));
		this.container.addEventListener('mouseover', this._onMouseOver.bind(this));
		this.container.addEventListener('mouseleave', this._onMouseLeave.bind(this));
		return this;
	}

	_onMouseOver (e) {
		if (e.target.classList.contains('onhover')) {
			try {
				$(e.target)
					.closest('.dropdown')
					.children('.dropdown__list')
					.finish()
					.slideDown(200);
			} catch (e) {}
		}
	}

	_onClick (e) {
		var parent, menu;
		if (e.target.classList.contains('submenu')) {
			e.stopPropagation();
		}
		parent = $(e.target).closest('.dropdown').get(0);
		if (parent.dataset && parent.dataset.menu) {
			try {
				$('#' + parent.dataset.menu)
					.finish()
					.slideToggle(200);
			} catch (e) {}
		} else {
			$(parent)
				.children('.dropdown__list')
				.finish()
				.slideToggle(200);
		}
	}

	_onMouseLeave (e) {
		this.close.call(this);
	}

	close () {
		$(this.container)
			.children('.dropdown__list')
			.finish()
			.slideUp(200);
	}
}
import { CustomForm } from './customform';
import { SpinnerInput } from './spinnerinput';
import { DropDownInput } from './dropdowninput';

export default class AdapterForm  extends CustomForm {
    constructor (form) {
        super(form);
        this.spinners = this.form.querySelectorAll(".wrap__spinner");
        this.dropdowns = this.form.querySelectorAll(".wrap__dropdown");
        this.total = this.form.querySelector(".adapter__total_sum");
        this.reducer = (accumulator, currentValue) => accumulator + currentValue;
    }

    init (app=null) {
        super.init.call(this, app);
        this.spinners = Array.from(this.spinners).map(i => new SpinnerInput (i).init());
        this.dropdowns = Array.from(this.dropdowns).map(i => new DropDownInput (i).init());
        this.form.addEventListener('change', this._onChange.bind(this));
        return this;
    }

    closeAllDropDowns () {
        this.dropdowns.forEach(d => d.close());
    }

    _onChange (e) {
        var selectedDevice = this.form.elements["device"].value;
        if (e.target.name === 'has_jack') {
            $(this.form).toggleClass('jack_adapter_needed-' + selectedDevice, e.target.value !== 'yes');
            $(this.form).removeClass((idx, classes) => {
                return classes
                    .split(" ")
                    .filter(c => c.startsWith('with-product-jack_adapter'))
                    .join(" ");
            });
            $(this.form).toggleClass('with-product-jack_adapter--' + selectedDevice, e.target.value !== 'yes');
        } else if (e.target.name === 'device') {
            var jackAdapterNeeded = this.form.elements["has_jack"].value;
            $(this.form).removeClass((i, classes) => {
                return classes
                    .split(" ")
                    .filter(c => c.startsWith('jack_adapter_needed'))
                    .join(" ");
            });
            $(this.form).toggleClass('jack_adapter_needed-' + e.target.value, jackAdapterNeeded !== 'yes');
            $(this.form).removeClass((idx, classes) => {
                return classes
                    .split(" ")
                    .filter(c => c.startsWith('with-product-jack_adapter'))
                    .join(" ");
            });
            $(this.form).toggleClass('with-product-jack_adapter--' + selectedDevice, jackAdapterNeeded !== 'yes');
        } else {
            $(this.form).removeClass((idx, classes) => {
                return classes
                    .split(" ")
                    .filter(c => c.startsWith('with-product-wellness'))
                    .join(" ");
            });
            $(this.form).addClass('with-product-' + this.form.elements['product'].value);
        }
        this._recalc.call(this);
    }

    _recalc() {
        var self = this;
        var total = Array
            .from(this.form.elements)
            .filter(e => e.dataset.item && e.checked)
            .map(e => {
                var name = e.dataset.item;
                var quantity = self.form.elements['quantity[\'' + name + '\']'];
                var multiplier = Array.from(self.form.elements).filter(e => {
                    var state = e.checked && e.dataset.multiplier;
                    state = state && JSON.parse(e.dataset.multiplier).includes(name);
                    return state;
                });
                if(multiplier.length) {
                    return e.dataset.price * quantity.value * multiplier[0].dataset.koeff;
                } else {
                    return e.dataset.price * quantity.value;
                }

            })
            .reduce(this.reducer);
        this.total.innerText = total;
    }
}
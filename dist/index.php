<? include $_SERVER['DOCUMENT_ROOT'].'/data.php'; ?>
<? include $_SERVER['DOCUMENT_ROOT'].'/header_index.php' ?>
	<section class="about">
		<div class="container">
			<div class="about__container">
				<div class="about__banner"></div>
				<div class="about__text_block">
					<h2 class="about__header">
						About Yet app
					</h2>
					<?foreach($paragraphs as $paragraph) { ?>
						<h6>
							<?=$paragraph['heading']?>
						</h6>
						<p>
							<?=$paragraph['text']?>
						</p>
					<? } ?>
				</div>
			</div>
		</div>
	</section>
	<section id="calendar" class="calendar">
		<div class="container">
			<div class="calendar__container">
				<div class="calendar__text_block">
					<h2 class="calendar__heading">
						Calendar
					</h2>
					<h6>
						The calendar for two types of a cycle, at monthly or at a menopause.
					</h6>
					<p>
						The calendar at monthly allows to trace a menstrual cycle, to receive notices about the forthcoming monthly and to celebrate days with feeling sick. In days of unpleasant symptoms before periods or during monthly Yet Wellness for improvement of the general health will be offered. The calendar at a menopause allows to celebrate days with feeling sick and to add the notes. In days with feeling sick Yet Wellness will be offered.
					</p>
				</div>
				<div class="calendar__banner"></div>
			</div>
		</div>
	</section>
	<section id="wellness" class="wellness">
		<div class="container">
			<div class="wellness__container">
				<div class="wellness__banner"></div>
				<div class="wellness__text_block">
					<h2 class="wellness__header">
						Wellness
					</h2>
					<p>
						Yet Wellness is a periodically repeated session of maintenance of good health in the period of monthly or menopauses with use of Yet Wellness wave. Yet Wellness wave is a wave of harmony of a female body which improves the general health at unpleasant symptoms in the period of monthly or menopauses. Yet Wellness wave it is developed by method of the spectral analysis and now by means of our application yours the smartphone can generate this wave of harmony for you.
					</p>
                    <h6>To use the necessary bracelet or adapter</h6>
					<a class="btn wellness__buy" href="/buy/">BUY NOW</a>
				</div>
			</div>
		</div>
	</section>
	<section id="monitor" class="monitor">
		<div class="container">
			<div class="monitor__container">
				<div class="monitor__text_block">
					<h2 class="monitor__header">
						Women’s health monitor
					</h2>
					<p>
						Women’s health monitor is primary screening the test of a state female health on gynecology and a mammary gland. On special algorithms in periodically repeated screening the questionnaire is defined primary condition of women's health on gynecology and a mammary gland. At detection aberrations the recommendation to address will be made to the doctor, it will allow to receive treatment in due time in case of need.
					</p>
				</div>
				<div class="monitor__banner"></div>
			</div>
		</div>
	</section>
	<section class="news">
		<div class="container news__container">
			<h3 class="news__header">
				News
			</h3>
			<div class="news__tiles">
				<? foreach ($news as $post) { ?>
					<div class="news__preview cover-link">
						<div class="news__preview_picture cover-image filled">
							<img src="<?=$post['image']?>">
						</div>
						<p class="news__anons">
							<?=$post['text']['preview']?>
						</p>
						<a href="/news/<?=$post['code']?>.php"></a>
					</div>
				<? } ?>
			</div>
			<button class="btn btn-light news__load_more">MORE NEWS</button>
		</div>
	</section>
<? include $_SERVER['DOCUMENT_ROOT'].'/footer_index.php' ?>
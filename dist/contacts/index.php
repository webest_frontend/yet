<? include $_SERVER['DOCUMENT_ROOT'] . '/header.php' ?>
    <section class="contacts">
        <div class="container">
            <nav class="breadcrumbs">
                <ul class="breadcrumbs__chain">
                    <li class="cover-link">
                        Home
                        <a href="/"></a>
                    </li>
                    <li>
                        Contacts
                    </li>
                </ul>
            </nav>
            <h1 class="contacts__header">Contacts</h1>
            <h2>If you have questions regarding the app or its functions&nbsp;please do the following</h2>
            <ol>
                <li>launch the app on your mobile device</li>
                <li>go to More/Contact Support</li>
                <li>tell us what bothers you in as much detail as possible</li>
            </ol>
            <p>You can also contact our support team via email: <a href="mailto:support@flo.health">support@flo.health</a></p>
            <h2>For general enquiries:</h2>
            <p><a href="mailto:info@flo.health">info@flo.health</a></p>
            <h3>If you have any questions or concerns about your privacy, any provisions of our Privacy Policy or any of your rights, you may contact us at:</h3>
            <p>Flo Health Inc. 1013 Centre Road, Suite 403‑B Wilmington, DE 19805 Email: <a href="mailto:support@flo.health">support@flo.health</a> or <a href="mailto:dpo@flo.health">dpo@flo.health</a></p>
            <h2>Our EU representative:</h2>
            <p>
                <strong>DPOEU LTD</strong><br>
                Office 902, Oval, Krinou 3, Ayios Athanasios, 4103, Limassol, Cyprus<br>
                Email:&nbsp;<a href="mailto:info@dpoeu.eu">info@dpoeu.eu</a>
            </p>
        </div>
    </section>
<? include $_SERVER['DOCUMENT_ROOT'] . '/footer.php' ?>
<?include $_SERVER['DOCUMENT_ROOT'].'/header.php'?>
<section class="adapter">
	<div class="container">
		<nav class="breadcrumbs">
			<ul class="breadcrumbs__chain">
				<li class="cover-link">
					Home
					<a href="/"></a>
				</li>
				<li>Adapter</li>
			</ul>
		</nav>
		<h3 class="adapter__header">Adapter</h3>
		<form id="order-form" class="adapter__form with-product-wellness-adapter" action="#" data-modal="submit-info">
			<div class="adapter__buy">
				<div class="adapter__step">Choose an adapter or bracelet:</div>
				<div class="adapter__tiles">
					<label class="adapter__tile wrap wrap__radio">
						<input type="radio" name="product" value="wellness-adapter" data-item="adapter" data-price="49" checked>
						<div class="adapter__tile_border">
							<img src="/images/adapter.png">
						</div>
						<span class="adapter__tile_data name">WELLNESS-ADAPTER</span>
						<span class="adapter__tile_data price">$49</span>
						<span class="checkmark"></span>
					</label>
					<label class="adapter__tile wrap wrap__radio">
						<input type="radio" name="product" value="wellness-bracelet" data-item="bracelet" data-price="99">
						<div class="adapter__tile_border">
                            <img src="/images/bracelet.png">
                        </div>
						<span class="adapter__tile_data name">PRE-ORDER WELLNESS-BRACELET + FREE WELLNESS-ADAPTER</span>
						<span class="adapter__tile_data price">$99</span>
						<span class="adapter__tile_data quantity">399 штук</span>
						<span class="checkmark"></span>
					</label>
				</div>
			</div>
			<div class="adapter__buy">
				<div class="adapter__step">Select your device:</div>
				<div class="adapter__device_select">
					<label class="wrap wrap__radio">
						iPhone
						<input type="radio" name="device" value="iphone" data-item="jack_adapter_iphone" data-price="10" checked>
						<span class="checkmark"></span>
					</label>
					<label class="wrap wrap__radio">
						Android
						<input type="radio" name="device" value="android" data-item="jack_adapter_android" data-price="10">
						<span class="checkmark"></span>
					</label>
				</div>
			</div>
			<div class="adapter__buy">
				<div class="adapter__step">У устройства есть разъем для наушников?</div>
				<div class="adapter__has_jack">
					<label class="wrap wrap__radio">
						Yes
						<input type="radio" name="has_jack" value="yes" data-multiplier='["jack_adapter_iphone", "jack_adapter_android"]' data-koeff=0 checked>
						<span class="checkmark"></span>
					</label>
					<label class="wrap wrap__radio">
						No
						<input type="radio" name="has_jack" value="no" data-multiplier='["jack_adapter_iphone", "jack_adapter_android"]' data-koeff=1>
						<span class="checkmark"></span>
					</label>
				</div>
				<p class="adapter__paragraph visible-iphone visible-android">Вам необходимо приобрести переходник. Мы добавили его в Ваш заказ</p>
				<div class="adapter__jack_adapter_preview cover-image visible-iphone">
					<img src="/images/adapter_iphone.png">
				</div>
				<div class="adapter__jack_adapter_preview cover-image visible-android">
					<img src="/images/adapter_android.png">
				</div>
			</div>
            <div data-product="product-bracelet">
                <div class="adapter__buy">
                    <div class="adapter__step">Выберите цвет браслета</div>
                    <div class="wrap wrap__dropdown">
                        <div class="wrap__dropdown_selected">Pink</div>
                        <div class="wrap__dropdown_list">
                            <label>
                                <span>Pink</span>
                                <input type="radio" name="color['bracelet']" value="pink" checked>
                                <span class="checkmark"></span>
                            </label>
                            <label>
                                <span>White</span>
                                <input type="radio" name="color['bracelet']" value="white">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
			<div class="adapter__buy">
				<div class="adapter__step">Ваш заказ</div>
				<div class="container-half">
                    <div data-product="product-bracelet">
                        <div class="adapter__order">
                            <div class="order__item">
                                <div class="order__preview">
                                    <img src="/images/bracelet_basket.png">
                                    <div class="order__data">
                                        Wellness-bracelet
                                    </div>
                                    <div class="order__price">$99</div>
                                </div>
                            </div>
                            <div class="order__spinner wrap wrap__spinner">
                                <input type="number" name="quantity['bracelet']" max="100" min="1" value="1">
                                <div class="minus"></div>
                                <div class="display">1</div>
                                <div class="plus"></div>
                            </div>
                        </div>
                    </div>
                    <div data-product="product-adapter">
                        <div class="adapter__order">
                            <div class="order__item">
                                <div class="order__preview">
                                    <img src="/images/adapter_basket.png">
                                    <div class="order__data">
                                        Wellness-adapter
                                    </div>
                                    <div class="order__price">$49</div>
                                </div>
                            </div>
                            <div class="order__spinner wrap wrap__spinner">
                                <input type="number" name="quantity['adapter']" max="100" min="1" value="1">
                                <div class="minus"></div>
                                <div class="display">1</div>
                                <div class="plus"></div>
                            </div>
                        </div>
                    </div>
                    <div data-product="product-jack_adapter--iphone">
                        <div class="adapter__order">
                            <div class="order__item">
                                <div class="order__preview">
                                    <img src="/images/iphone_jack_adapter_basket.png">
                                    <div class="order__data">
                                        audio adapter
                                    </div>
                                    <div class="order__price">$10</div>
                                </div>
                            </div>
                            <div class="order__spinner wrap wrap__spinner">
                                <input type="number" name="quantity['jack_adapter_iphone']" max="100" min="1" value="1">
                                <div class="minus"></div>
                                <div class="display">1</div>
                                <div class="plus"></div>
                            </div>
                        </div>
                    </div>
                    <div data-product="product-jack_adapter--android">
                        <div class="adapter__order">
                            <div class="order__item">
                                <div class="order__preview">
                                    <img src="/images/android_jack_adapter_basket.png">
                                    <div class="order__data">
                                        audio adapter
                                    </div>
                                    <div class="order__price">$10</div>
                                </div>
                            </div>
                            <div class="order__spinner wrap wrap__spinner">
                                <input type="number" name="quantity['jack_adapter_android']" max="100" min="1" value="1">
                                <div class="minus"></div>
                                <div class="display">1</div>
                                <div class="plus"></div>
                            </div>
                        </div>
                    </div>
                    <div class="adapter__total_block">
                        Total: $<span class="adapter__total_sum">49</span>
                    </div>
					<div class="adapter__inputs_container">
						<label class="wrap wrap__text">
			                <input name="name" type="text">
			                <span class="placeholder">Full name</span>
			                <span class="error-string"></span>
			            </label>
						<label class="wrap wrap__text">
			                <input name="address" type="text">
			                <span class="placeholder">Your address</span>
			                <span class="error-string"></span>
			            </label>
						<label class="wrap wrap__text">
			                <input name="city" type="text">
			                <span class="placeholder">City</span>
			                <span class="error-string"></span>
			            </label>
						<label class="wrap wrap__text">
			                <input name="state" type="text">
			                <span class="placeholder">State/Province/Region</span>
			                <span class="error-string"></span>
			            </label>
						<label class="wrap wrap__text">
			                <input name="zip" type="text">
			                <span class="placeholder">ZIP</span>
			                <span class="error-string"></span>
			            </label>
						<label class="wrap wrap__mask">
		                	<input name="phone" type="tel">
		                </label>
					</div>
					<div class="adapter__submit_block">
						<label class="wrap wrap__checkbox">
			            	Нажимая на кнопку, я даю согласие на обработку <a class="link_underlined" href="/policy_of_processing_of_personal_data.php" target="_blank">персональных данных</a>
							<input type="checkbox" name="agree" required checked>
		                    <span class="checkmark"></span>
						</label>
						<input type="submit" class="order__submit btn" value="pay">
					</div>
				</div>
			</div>
		</form>
	</div>
</section>
<?include $_SERVER['DOCUMENT_ROOT'].'/footer.php'?>
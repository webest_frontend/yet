<? include $_SERVER['DOCUMENT_ROOT'].'/header.php' ?>
<? include $_SERVER['DOCUMENT_ROOT'].'/data.php' ?>
<?
	$query = array_filter(
		$news,
		function ($p) {
			return $p['code'] == '4';
		}
	);
	if(!empty($query)) {
		$post = $query[array_keys($query)[0]];
	} else die();
?>
<section class="news-detail">
	<div class="container">
		<nav class="breadcrumbs">
			<ul class="breadcrumbs__chain">
				<li class="cover-link">
					Home
					<a href="/"></a>
				</li>
				<li class="cover-link">
					News
					<a href="/news.php"></a>
				</li>
				<li>News page</li>
			</ul>
		</nav>
		<h3 class="news-detail__header">Heading</h3>
		<div class="news-detail__meta">
			<span class="news-detail__date">
				<?=$post['date']?>
			</span>
			<button class="btn btn-tab news-detail__category">
				<?=$post['category']?>
			</button>
		</div>
		<div class="news-detail__content">
			<div class="news-detail__text">
				<?=$post['text']['detail']?>
			</div>
			<div class="news-detail__picture cover-image filled">
				<img src="<?=$post['image']?>">
			</div>
		</div>
		<div class="news__tiles">
			<? for ($i=0; $i<4; $i++) {
				$post = $news[$i]; ?>
				<div class="news__preview cover-link">
					<div class="news__preview_picture cover-image filled">
						<img src="<?=$post['image']?>">
					</div>
					<p class="news__preview_text">
						<?=$post['text']['preview']?>
					</p>
					<a href="/news/<?=$post['code']?>.php"></a>
				</div>
			<? } ?>
		</div>
	</div>
</section>
<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php' ?>
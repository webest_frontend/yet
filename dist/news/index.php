<? include $_SERVER['DOCUMENT_ROOT'].'/header.php' ?>
<? include $_SERVER['DOCUMENT_ROOT'].'/data.php' ?>
<section class="news">
	<div class="container">
		<nav class="breadcrumbs">
			<ul class="breadcrumbs__chain">
				<li class="cover-link">
					Home
					<a href="/"></a>
				</li>
				<li>
					News
				</li>
			</ul>
		</nav>
		<h3 class="news__header">News</h3>
		<div class="news__tiles">
			<? foreach ($news as $post) { ?>
				<div class="news__preview cover-link">
					<div class="news__preview_picture cover-image filled">
						<img src="<?=$post['image']?>">
					</div>
					<p class="news__preview_text">
						<?=$post['text']['preview']?>
					</p>
					<a href="/news/<?=$post['code']?>.php"></a>
				</div>
			<? } ?>
		</div>
		<button class="btn btn-light news__load_more">more news</button>
	</div>
</section>		
<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php' ?>
<!DOCTYPE html>
<html lang='en'>
<head>
    <title>Yet app</title>
    <base href="/">
    <link rel="stylesheet" type="text/css" href="css/app.css?ver=1.84">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link type="image/x-icon" href="favicon.ico" rel="shortcut icon">
    <link rel="manifest" href="manifest.json">
</head>
<body>
<header class="header">
    <nav class="header__navbar">
        <div class="container">
            <div class="menu__tablet dropdown laptop-visible mobile-hidden">MENU
                <svg>
                    <use xlink:href="images/icons/sprite.svg#hamburger"></use>
                </svg>
                <ul class="dropdown__list">
                    <li data-section="calendar">
                        CALENDAR
                    </li>
                    <li data-section="wellness">
                        WELLNESS
                    </li>
                    <li data-section="monitor">
                        MONITOR
                    </li>
                    <li class="cover-link">
                        ADAPTER / BRACELET
                        <a href="/adapter-bracelet/"></a>
                    </li>
                    <li class="cover-link">
                        NEWS
                        <a href="/news/"></a>
                    </li>
                    <li class="cover-link dropdown submenu">
                        PRIVACY
                        <ul class="dropdown__list">
                            <li class="cover-link">
                                Terms of use
                                <a href="/privacy/terms_of_use/"></a>
                            </li>
                            <li class="cover-link">
                                Processing of personal data
                                <a href="/privacy/policy_of_processing_of_personal_data/"></a>
                            </li>
                            <li class="cover-link">
                                Provisions of a renewable paid subscription
                                <a href="/privacy/provisions_of_a_renewable_paid_subscription/"></a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="menu__mobile dropdown mobile-visible" data-menu="overlay__menu">
                <svg>
                    <use xlink:href="images/icons/sprite.svg#hamburger"></use>
                </svg>
            </div>
            <a class="header__logo" href="/"></a>
            <ul class="header__menu menu__desktop laptop-hidden">
                <li class="cover-link" data-section="calendar">
                    CALENDAR
                </li>
                <li class="cover-link" data-section="wellness">
                    WELLNESS
                </li>
                <li class="cover-link" data-section="monitor">
                    MONITOR
                </li>
                <li class="cover-link">
                    ADAPTER / BRACELET
                    <a href="/adapter-bracelet/"></a>
                </li>
                <li class="cover-link">
                    NEWS
                    <a href="/news/"></a>
                </li>
                <li class="cover-link dropdown onhover">
                    PRIVACY
                    <ul class="dropdown__list">
                        <li class="cover-link">
                            Terms of use
                            <a href="/privacy/terms_of_use/"></a>
                        </li>
                        <li class="cover-link">
                            Processing of personal data
                            <a href="/privacy/policy_of_processing_of_personal_data/"></a>
                        </li>
                        <li class="cover-link">
                            Provisions of a renewable paid subscription
                            <a href="/privacy/provisions_of_a_renewable_paid_subscription/"></a>
                        </li>
                    </ul>
                </li>
            </ul>
            <div class="shields mobile-hidden">
                <div class="shield cover-link">
                    <img src="images/icons/app_store.svg">
                    <a href="#"></a>
                </div>
                <div class="shield cover-link">
                    <img src="images/icons/google_play.svg">
                    <a href="#"></a>
                </div>
            </div>
            <a class="btn btn-light mobile-hidden" href="/buy/">BUY NOW</a>
        </div>
    </nav>
</header>
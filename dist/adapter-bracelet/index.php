<?include $_SERVER['DOCUMENT_ROOT'].'/header.php'?>
<?include $_SERVER['DOCUMENT_ROOT'].'/data.php'?>
<section class="catalog">
	<div class="container">
		<nav class="breadcrumbs">
			<ul class="breadcrumbs__chain">
				<li class="cover-link">
					Home
					<a href="/"></a>
				</li>
				<li>Adapters / bracelet</li>
			</ul>
		</nav>
		<h3 class="catalog__header">Adapters</h3>
        <div class="catalog__slider slider--adapter">
            <? foreach ($slider['adapter'] as $slide) { ?>
                <div class="catalog__slide">
                    <img src="<?= $slide ?>">
                </div>
            <? } ?>
        </div>
        <h3 class="catalog__header">Bracelet</h3>
        <div class="catalog__slider slider--bracelet">
            <? foreach ($slider['bracelet'] as $slide) { ?>
                <div class="catalog__slide">
                    <img src="<?= $slide ?>">
                </div>
            <? } ?>
        </div>
    </div>
</section>
<?include $_SERVER['DOCUMENT_ROOT'].'/footer.php'?>
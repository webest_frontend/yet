<div class="overlay_content" id="overlay__menu">
	<div class="menu__mobile_header">
        <div class="menu__mobile_close_btn cover-link">
            <svg>
                <use xlink:href="images/icons/sprite.svg#hamburger"></use>
            </svg>
        </div>
		<a class="btn btn-light" href="/buy/">BUY ADAPTER</a>
	</div>
	<ul class="menu__mobile_menu">
		<li class="cover-link" data-section="calendar">
			CALENDAR
		</li>
		<li class="cover-link" data-section="wellness">
			WELLNESS
		</li>
		<li class="cover-link" data-section="monitor">
			MONITOR
		</li>
		<li class="cover-link">
			ADAPTER / BRACELET
			<a href="/adapter-bracelet/"></a>
		</li>
		<li class="cover-link">
			NEWS
			<a href="/news/"></a>
		</li>
		<li class="dropdown submenu">
			PRIVACY
			<ul class="dropdown__list">
				<li class="cover-link">
                    TERMS_OF_USE
					<a href="/privacy/terms_of_use/"></a>
				</li>
				<li class="cover-link">
                    POLICY_OF_PROCESSING_OF_PERSONAL_DATA
					<a href="/privacy/policy_of_processing_of_personal_data/"></a>
				</li>
				<li class="cover-link">
                    PROVISIONS_OF_A_RENEWABLE_PAID_SUBSCRIPTION
					<a href="/privacy/provisions_of_a_renewable_paid_subscription/"></a>
				</li>
			</ul>
		</li>
	</ul>
	<div class="menu__mobile_footer shields">
		<div class="shield cover-link">
			<svg>
                <use xlink:href="images/icons/sprite.svg#app_store"></use>
            </svg>
            <a href="#"></a>
		</div>
		<div class="shield cover-link">
			<img src="/images/icons/google_play.svg">
            <a href="#"></a>
		</div>
	</div>
</div>

<div class="modal mfp-hide" id="buying_adapter">
	<h6>Buying an adapter</h6>
	<div class="package">
		<div class="package__preview">
			<div class="package__title">Package 1</div>
			<p class="package__description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</p>
		</div>
		<div class="package__price">
			150 $
		</div>
	</div>
	<form id="buying_adapter_form" data-modal="submit-info">
		<div class="inputs_container">
			<label class="wrap wrap__text">
	            <input name="name" type="text">
	            <span class="placeholder">Your name</span>
	            <span class="error-string"></span>
	        </label>
			<label class="wrap wrap__text">
	            <input name="email" type="email">
	            <span class="placeholder">Your e-mail</span>
	            <span class="error-string"></span>
	        </label>
			<label class="wrap wrap__mask">
	            <input name="phone" type="tel">
	        </label>
		</div>
		<label class="wrap wrap__checkbox">
        	Нажимая на кнопку, я даю согласие на обработку <a href="/policy_of_processing_of_personal_data.php">персональных данных</a>
			<input type="checkbox" name="agree" required checked>
            <span class="checkmark"></span>
		</label>
		<input class="btn" type="submit" value="send">
	</form>
</div>

<div class="modal mfp-hide" id="submit-info">
	<h6>Payment successfully took place</h6>
	<p>
		Your order is taken! Thank you for choosing us.
	</p>
</div>

<div id="preloader">
	<div class="lds-ring">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
</div>
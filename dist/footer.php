	<section class="get_app">
		<div class="container">
			<div class="get_app__container">
				<div class="get_app__banner"></div>
				<div class="get_app__text_block">
					<h3 class="get_app__header">
						Track your health with Yet App
					</h3>
					<p>
                        <ul class="get_app__app_features">
                            <li>Calendar - events and reminders</li>
                            <li>Wellness - activity and harmony</li>
                            <li>Monitor - control and care</li>
                        </ul>
					</p>
					<div class="shields">
						<div class="shield cover-link">
							<img src="images/icons/app_store.svg">
		                    <a href="#"></a>
						</div>
						<div class="shield cover-link">
							<img src="images/icons/google_play.svg">
		                    <a href="#"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<footer class="footer">
		<div class="footer__top">
			<div class="container">
				<div class="footer__top_container">
					<ul class="footer__menu">
						<li class="cover-link">
							Contacts
							<a href="/contacts/"></a>
						</li>
						<li class="cover-link">
							F.A.Q.
							<a href="/faq/"></a>
						</li>
					</ul>
					<div class="footer__top_right">
						<ul class="footer__socials">
							<li class="cover-link">
								<svg>
			                        <use xlink:href="images/icons/sprite.svg#insta"></use>
			                    </svg>
			                    <a href="#"></a>
							</li class="cover-link">
							<li class="cover-link">
								<svg>
			                        <use xlink:href="images/icons/sprite.svg#facebook"></use>
			                    </svg>
			                    <a href="#"></a>
							</li>
							<li class="cover-link">
								<svg>
			                        <use xlink:href="images/icons/sprite.svg#twitter"></use>
			                    </svg>
			                    <a href="#"></a>
							</li>
							<li class="cover-link">
								<svg>
			                        <use xlink:href="images/icons/sprite.svg#linkedin"></use>
			                    </svg>
			                    <a href="#"></a>
							</li>
						</ul>
						<a class="btn btn-light" href="/buy/">BUY NOW</a>
					</div>
				</div>
			</div>
		</div>
		<div class="footer__bottom">
			<div class="container">
				<ul class="footer__terms">
					<li>
						@ Yet <?=date("Y")?>
					</li>
					<li class="link_underlined cover-link">
						Application terms of use
						<a href="/privacy/terms_of_use/"></a>
					</li>
					<li class="link_underlined cover-link">
						Privacy Policy
						<a href="/privacy/policy_of_processing_of_personal_data/"></a>
					</li>
					<li class="link_underlined cover-link">
						Provisions of a renewable paid subscription
						<a href="/privacy/provisions_of_a_renewable_paid_subscription/"></a>
					</li>
				</ul>
			</div>
		</div>
	</footer>
	<? include $_SERVER['DOCUMENT_ROOT'].'/overlay.php' ?>
	<script src="<?$_SERVER['DOCUMENT_ROOT']?>/js/vendor.app.js?ver=1.09"></script>
	<script src="<?$_SERVER['DOCUMENT_ROOT']?>/js/app.js?ver=1.34"></script>
</body>
</html>
<?include $_SERVER['DOCUMENT_ROOT'].'/header.php'?>
<?include $_SERVER['DOCUMENT_ROOT'].'/data.php'?>
<section class="faq">
	<div class="container">
		<nav class="breadcrumbs">
			<ul class="breadcrumbs__chain">
				<li class="cover-link">
					Home
					<a href="/"></a>
				</li>
				<li>
					FAQ
				</li>
			</ul>
		</nav>
		<h2 class="adapter__header">F.A.Q.</h2>
		<div class="faq__tabs">
			<ul class="faq__nav">
				<li class="btn btn-tab active" data-id="tab_all">All</li>
				<li class="btn btn-tab" data-id="tab_wellness">Wellness</li>
				<li class="btn btn-tab" data-id="tab_monitor">Monitor</li>
				<li class="btn btn-tab" data-id="tab_calendar">Calendar</li>
				<li class="btn btn-tab" data-id="tab_wellness_adapter">Wellness adapter</li>
			</ul>
			<?foreach ([
				'tab_all',
				'tab_wellness',
				'tab_monitor',
				'tab_calendar',
				'tab_wellness_adapter'
			] as $tab) { ?>
				<div id="<?=$tab?>" class="faq__panel accordion">
					<?foreach ($faqs as $faq) { ?>
						<div class="accordion__set">
							<div class="accordion__header">
								<?=$faq['question']?>
							</div>
							<div class="accordion__content">
								<?=$faq['answer']?>
							</div>
						</div>
					<? } ?>
				</div>
			<? } ?>
		</div>	
		<button class="faq__load_more btn btn-light">To load more</button>
	</div>
</section>
<?include $_SERVER['DOCUMENT_ROOT'].'/footer.php'?>
<?include $_SERVER['DOCUMENT_ROOT'].'/header.php'?>
<section class="not-found">
	<div class="container">
		<div class="not-found__container">
			<div class="not-found__banner cover-image">
				<img src="/images/404.png">
			</div>
			<h3 class="not-found__header">
				The page is not found or not started.
			</h3>
			<p>
				If you the owner of this page also do not know how to correct a situation, information can be obtained in article or to address to technical support of the LP Platform
			</p>
		</div>
	</div>
</section>
<?include $_SERVER['DOCUMENT_ROOT'].'/footer.php'?>
(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["adapter_form"],{

/***/ "./src/js/adapterform.js":
/*!*******************************!*\
  !*** ./src/js/adapterform.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return AdapterForm; });
/* harmony import */ var _customform__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./customform */ "./src/js/customform.js");
/* harmony import */ var _spinnerinput__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./spinnerinput */ "./src/js/spinnerinput.js");
/* harmony import */ var _dropdowninput__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./dropdowninput */ "./src/js/dropdowninput.js");




class AdapterForm  extends _customform__WEBPACK_IMPORTED_MODULE_0__["CustomForm"] {
    constructor (form) {
        super(form);
        this.spinners = this.form.querySelectorAll(".wrap__spinner");
        this.dropdowns = this.form.querySelectorAll(".wrap__dropdown");
        this.total = this.form.querySelector(".adapter__total_sum");
        this.reducer = (accumulator, currentValue) => accumulator + currentValue;
    }

    init (app=null) {
        super.init.call(this, app);
        this.spinners = Array.from(this.spinners).map(i => new _spinnerinput__WEBPACK_IMPORTED_MODULE_1__["SpinnerInput"] (i).init());
        this.dropdowns = Array.from(this.dropdowns).map(i => new _dropdowninput__WEBPACK_IMPORTED_MODULE_2__["DropDownInput"] (i).init());
        this.form.addEventListener('change', this._onChange.bind(this));
        return this;
    }

    closeAllDropDowns () {
        this.dropdowns.forEach(d => d.close());
    }

    _onChange (e) {
        var selectedDevice = this.form.elements["device"].value;
        if (e.target.name === 'has_jack') {
            $(this.form).toggleClass('jack_adapter_needed-' + selectedDevice, e.target.value !== 'yes');
            $(this.form).removeClass((idx, classes) => {
                return classes
                    .split(" ")
                    .filter(c => c.startsWith('with-product-jack_adapter'))
                    .join(" ");
            });
            $(this.form).toggleClass('with-product-jack_adapter--' + selectedDevice, e.target.value !== 'yes');
        } else if (e.target.name === 'device') {
            var jackAdapterNeeded = this.form.elements["has_jack"].value;
            $(this.form).removeClass((i, classes) => {
                return classes
                    .split(" ")
                    .filter(c => c.startsWith('jack_adapter_needed'))
                    .join(" ");
            });
            $(this.form).toggleClass('jack_adapter_needed-' + e.target.value, jackAdapterNeeded !== 'yes');
            $(this.form).removeClass((idx, classes) => {
                return classes
                    .split(" ")
                    .filter(c => c.startsWith('with-product-jack_adapter'))
                    .join(" ");
            });
            $(this.form).toggleClass('with-product-jack_adapter--' + selectedDevice, jackAdapterNeeded !== 'yes');
        } else  {
            $(this.form).removeClass((idx, classes) => {
                return classes
                    .split(" ")
                    .filter(c => c.startsWith('with-product-wellness'))
                    .join(" ");
            });
            $(this.form).addClass('with-product-' + this.form.elements['product'].value);
        }
        this._recalc.call(this);
    }

    _recalc() {
        var self = this;
        var total = Array
            .from(this.form.elements)
            .filter(e => e.dataset.item && e.checked)
            .map(e => {
                var name = e.dataset.item;
                var quantity = self.form.elements['quantity[\'' + name + '\']'];
                var multiplier = Array.from(self.form.elements).filter(e => {
                    var state = e.checked && e.dataset.multiplier;
                    state = state && JSON.parse(e.dataset.multiplier).includes(name);
                    return state;
                });
                if(multiplier.length) {
                    return e.dataset.price * quantity.value * multiplier[0].dataset.koeff;
                } else {
                    return e.dataset.price * quantity.value;
                }

            })
            .reduce(this.reducer);
        this.total.innerText = total;
    }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "../../../Users/Webest/node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./src/js/dropdowninput.js":
/*!*********************************!*\
  !*** ./src/js/dropdowninput.js ***!
  \*********************************/
/*! exports provided: DropDownInput */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DropDownInput", function() { return DropDownInput; });
class DropDownInput {

    constructor (container) {
        this.container = container;
        this.inputs = this.container.querySelectorAll('input');
        this.selected = this.container.querySelector('.wrap__dropdown_selected');
    }

    init () {
        this.container.addEventListener('click', this._onClick.bind(this));
        return this;
    }

    _onClick (e) {
        if(e.target instanceof HTMLInputElement) return;
        e.stopPropagation();
        this.container.classList.toggle('expanded');
        var label = e.target.closest('label');
        if (label) {
            this.selected.innerText = label.querySelector('span').innerText;
        }
    }

    close() {
        this.container.classList.remove('expanded');
    }

    reset () {
        this.inputs.forEach(i => {
            i.checked = false;
        });
        this.inputs.item(0).checked = true;
        this.selected.innerText = this.inputs.item(0).value;
    }
}

/***/ }),

/***/ "./src/js/spinnerinput.js":
/*!********************************!*\
  !*** ./src/js/spinnerinput.js ***!
  \********************************/
/*! exports provided: SpinnerInput */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpinnerInput", function() { return SpinnerInput; });
class SpinnerInput {

    constructor (container) {
        this.container = container;
        this.input = this.container.querySelector('input');
        this.plus = this.container.querySelector('.plus');
        this.minus = this.container.querySelector('.minus');
        this.display = this.container.querySelector('.display');
    }

    init () {
        this.container.addEventListener('click', this._onClick.bind(this));
        return this;
    }

    _onClick (e) {
        if (e.target.classList.contains('plus')) {
            this.input.value = Math.min(parseInt(this.input.value) + 1, this.input.max);
        } else if (e.target.classList.contains('minus')) {
            this.input.value = Math.max(parseInt(this.input.value) - 1, 1);
        }
        this.display.innerText = this.input.value;
        this.input.dispatchEvent(new Event('change', {bubbles: true}));
    }

    reset () {
        $(this.container).removeClass('valid invalid');
    }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "../../../Users/Webest/node_modules/jquery/dist/jquery.js")))

/***/ })

}]);
//# sourceMappingURL=adapter_form.app.js.map
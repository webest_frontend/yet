/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"main": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// script path function
/******/ 	function jsonpScriptSrc(chunkId) {
/******/ 		return __webpack_require__.p + "" + chunkId + ".app.js"
/******/ 	}
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/ 	// This file contains only the entry chunk.
/******/ 	// The chunk loading function for additional chunks
/******/ 	__webpack_require__.e = function requireEnsure(chunkId) {
/******/ 		var promises = [];
/******/
/******/
/******/ 		// JSONP chunk loading for javascript
/******/
/******/ 		var installedChunkData = installedChunks[chunkId];
/******/ 		if(installedChunkData !== 0) { // 0 means "already installed".
/******/
/******/ 			// a Promise means "currently loading".
/******/ 			if(installedChunkData) {
/******/ 				promises.push(installedChunkData[2]);
/******/ 			} else {
/******/ 				// setup Promise in chunk cache
/******/ 				var promise = new Promise(function(resolve, reject) {
/******/ 					installedChunkData = installedChunks[chunkId] = [resolve, reject];
/******/ 				});
/******/ 				promises.push(installedChunkData[2] = promise);
/******/
/******/ 				// start chunk loading
/******/ 				var script = document.createElement('script');
/******/ 				var onScriptComplete;
/******/
/******/ 				script.charset = 'utf-8';
/******/ 				script.timeout = 120;
/******/ 				if (__webpack_require__.nc) {
/******/ 					script.setAttribute("nonce", __webpack_require__.nc);
/******/ 				}
/******/ 				script.src = jsonpScriptSrc(chunkId);
/******/
/******/ 				// create error before stack unwound to get useful stacktrace later
/******/ 				var error = new Error();
/******/ 				onScriptComplete = function (event) {
/******/ 					// avoid mem leaks in IE.
/******/ 					script.onerror = script.onload = null;
/******/ 					clearTimeout(timeout);
/******/ 					var chunk = installedChunks[chunkId];
/******/ 					if(chunk !== 0) {
/******/ 						if(chunk) {
/******/ 							var errorType = event && (event.type === 'load' ? 'missing' : event.type);
/******/ 							var realSrc = event && event.target && event.target.src;
/******/ 							error.message = 'Loading chunk ' + chunkId + ' failed.\n(' + errorType + ': ' + realSrc + ')';
/******/ 							error.type = errorType;
/******/ 							error.request = realSrc;
/******/ 							chunk[1](error);
/******/ 						}
/******/ 						installedChunks[chunkId] = undefined;
/******/ 					}
/******/ 				};
/******/ 				var timeout = setTimeout(function(){
/******/ 					onScriptComplete({ type: 'timeout', target: script });
/******/ 				}, 120000);
/******/ 				script.onerror = script.onload = onScriptComplete;
/******/ 				document.head.appendChild(script);
/******/ 			}
/******/ 		}
/******/ 		return Promise.all(promises);
/******/ 	};
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// on error function for async loading
/******/ 	__webpack_require__.oe = function(err) { console.error(err); throw err; };
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./src/index.js","vendor"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ "../../../Users/Webest/node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var magnific_popup__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! magnific-popup */ "../../../Users/Webest/node_modules/magnific-popup/dist/jquery.magnific-popup.js");
/* harmony import */ var magnific_popup__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(magnific_popup__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var jquery_mask_plugin__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jquery-mask-plugin */ "../../../Users/Webest/node_modules/jquery-mask-plugin/dist/jquery.mask.js");
/* harmony import */ var jquery_mask_plugin__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jquery_mask_plugin__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var slick_carousel__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! slick-carousel */ "../../../Users/Webest/node_modules/slick-carousel/slick/slick.js");
/* harmony import */ var slick_carousel__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(slick_carousel__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _js_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./js/bootstrap */ "./src/js/bootstrap.js");

window.$ = window.jQuery = jquery__WEBPACK_IMPORTED_MODULE_0___default.a;





/***/ }),

/***/ "./src/js/accordion.js":
/*!*****************************!*\
  !*** ./src/js/accordion.js ***!
  \*****************************/
/*! exports provided: Accordion */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Accordion", function() { return Accordion; });
class Accordion {
    constructor (container) {
        this.container = container;
    }

    init () {
        this.container.addEventListener('click', this._onClick.bind(this));
        return this;
    }

    _onClick (e) {
        const target = $(e.target).closest('.accordion__set');
        if (target.length) {
            $(target)
                .toggleClass('expanded')
                .children('.accordion__content')
                .finish()
                .slideToggle(200);
        }
    }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "../../../Users/Webest/node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./src/js/app.js":
/*!***********************!*\
  !*** ./src/js/app.js ***!
  \***********************/
/*! exports provided: App */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "App", function() { return App; });
class App {

    constructor (container, components) {
        this.container = container;
        this.components = components;
        this.preloader = document.querySelector('#preloader');
        this.navbarOffcetHeight = 124;
        this.scrolledDown = false;
        this.modalConfig = {
            closeBtnInside: true,
            mainClass: 'mfp-fade'
        };
    }

    init () {
        var self = this;
    	for (let i in this.components) {
    		if (Array.isArray(this.components[i])) {
                this.components[i].forEach(c => c.init(self));
            } else {
                this.components[i].init(this);
            }
    	}
        this.container.addEventListener('click', this._onClick.bind(this));
        window.addEventListener('scroll', this._onScroll.bind(this));
        document.querySelectorAll('.modal-trigger').forEach(e => {
            $(e).magnificPopup(self.modalConfig);
        });
        return this;

    }

    _onClick (e) {
        var exception, parent;
        parent = $(e.target).closest('.dropdown').get(0);
        exception = $(parent).children('.dropdown__list');
        if (parent && parent.dataset && parent.dataset.menu) {
            exception = $(exception).add('#' + parent.dataset.menu);
        }
        if (e.target.dataset && e.target.dataset.section) {
            scrollToSection.call(e.target);
        }
        $('.dropdown__list, .overlay_content')
            .not(exception)
            .finish()
            .slideUp(200);
        try {
            this.components.adapterform.closeAllDropDowns();
        } catch (e) {}

        function scrollToSection () {
            const section = this.dataset.section;
            try {
                document
                    .getElementById(section)
                    .scrollIntoView({behavior: 'smooth'});
            } catch (e) {
                location.assign('/index.php#' + section);
            }
        }
    }

    _onScroll (e) {
        if(this.scrolledDown) {
            if(window.scrollY <= this.navbarOffcetHeight) {
                document.body.classList.remove('sticked_header');
                this.scrolledDown = false;
            }
        } else if(window.scrollY > this.navbarOffcetHeight) {
            document.body.classList.add('sticked_header');
            this.scrolledDown = true;
        }
    }

    startLoader () {
        $(this.preloader).show();
    }

    stopLoader () {
        $(this.preloader).hide();
    }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "../../../Users/Webest/node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./src/js/bootstrap.js":
/*!*****************************!*\
  !*** ./src/js/bootstrap.js ***!
  \*****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var _app__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app */ "./src/js/app.js");
/* harmony import */ var _customform__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./customform */ "./src/js/customform.js");
/* harmony import */ var _faqtabs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./faqtabs */ "./src/js/faqtabs.js");
/* harmony import */ var _dropdownmenu__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./dropdownmenu */ "./src/js/dropdownmenu.js");
/* harmony import */ var _catalog_slider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./catalog_slider */ "./src/js/catalog_slider.js");





__webpack_require__.e(/*! import() | adapter_form */ "adapter_form").then(__webpack_require__.bind(null, /*! ./adapterform */ "./src/js/adapterform.js")).then(chunk => {
	//console.log(chunk);
	document.querySelectorAll('#order-form').forEach(e => {
		app.components.adapterform = new chunk.AdapterForm (e).init(app);
	});
});
var app;
$(function () {
	var components = {
		dropdowns: [],
		orderitem: [],
		sliders: []
	};
	document.querySelectorAll('#buying_adapter_form').forEach(e => {
		components.customform = new _customform__WEBPACK_IMPORTED_MODULE_1__["CustomForm"] (e);
	});
	document.querySelectorAll('.faq__tabs').forEach(e => {
		components.faqtabs = new _faqtabs__WEBPACK_IMPORTED_MODULE_2__["FAQTabs"] (e);
	});
	document.querySelectorAll('.dropdown').forEach(e => {
		components.dropdowns.push(new _dropdownmenu__WEBPACK_IMPORTED_MODULE_3__["DropDownMenu"] (e));
	});
	document.querySelectorAll('.catalog__slider').forEach(e => {
		components.sliders.push(new _catalog_slider__WEBPACK_IMPORTED_MODULE_4__["CatalogSlider"] (e));
	});
	app = new _app__WEBPACK_IMPORTED_MODULE_0__["App"] (document.body, components).init();
});


/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "../../../Users/Webest/node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./src/js/catalog_slider.js":
/*!**********************************!*\
  !*** ./src/js/catalog_slider.js ***!
  \**********************************/
/*! exports provided: CatalogSlider */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CatalogSlider", function() { return CatalogSlider; });
class CatalogSlider {
    constructor(container) {
        this.slider = container;
        this.slider_config = {
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            waitForAnimate: false,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 5000,
            pauseOnFocus: false,
            pauseOnHover: false
        };
    }

    init() {
        $(this.slider).slick(this.slider_config);
        return this;
    }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "../../../Users/Webest/node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./src/js/customform.js":
/*!******************************!*\
  !*** ./src/js/customform.js ***!
  \******************************/
/*! exports provided: CustomForm */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomForm", function() { return CustomForm; });
/* harmony import */ var _textinput__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./textinput */ "./src/js/textinput.js");


class CustomForm {
    constructor (form) {
        this.form = form;
        this.inputs = this.form.querySelectorAll(".wrap__text");
        this.maskedInputs = this.form.querySelectorAll('.wrap__mask');
        this.submitBtn = this.form.querySelector("[type='submit']");
        this.modal = this.form.dataset.modal;
        this.modalConfig = {
            items: {
                src: $('#' + this.modal),
                type: 'inline'
            },
            mainClass: 'mfp-fade',
            showCloseBtn: false
        };
    }

    init (app=null) {
        this.app = app;
        this.form.addEventListener('submit', this._onSubmit.bind(this));
        this.inputs = Array.from(this.inputs).map(i => new _textinput__WEBPACK_IMPORTED_MODULE_0__["TextInput"] (i).init());
        $(this.maskedInputs)
            .find('input')
            .mask("+0 (000)-000-00-00", {placeholder: "+ (___)-___-__-__"});
        return this;
    }

    _onSubmit (e) {
        const self = this;
        var responseText;
        e.preventDefault();
        this.submitBtn.disabled = true;
        this.app.startLoader();
        /*$.ajax({
            url: this.form.action,
            method: 'POST',
            dataType: 'json',
            contentType: 'application/x-www-form-urlencoded',
            data: $(this.form).serialize(),
            success: this._onSuccess.bind(this),
            error: this._onError.bind(this),
            complete: this._onComplete.bind(this)
        });*/
        responseText = "<div class='heading'>" +
                "Payment successfully took place" +
                "</div>" +
                "<p>" +
                    "Your order is taken! Thank you for choosing us." +
                "</p>";
        new Promise(resolve => {
            setTimeout(resolve.bind({}, {html: responseText}), 2000);
        })
        .then(res => {
            self._onSuccess.call(this, res);
        })
        .then(() => {
            self._onComplete.call(this);
        });
    }

    reset () {
        Array.from(this.inputs).forEach(i => i.reset());
        this.form.reset();
    }

    _onSuccess (response) {
        this.reset.call(this);
        $(this.modal)
            .toggleClass('error', false)
            .find('.dummy-container')
            .html(response.html);
    }

    _onError (XHR) {
        $(this.modal)
            .toggleClass('error', true)
            .find('.dummy-container')
            .text(XHR.responseJSON.message || XHR.responseText);
    }

    _onComplete() {
        $.magnificPopup.open(this.modalConfig);
        this.submitBtn.disabled = false;
        this.app.stopLoader();
    }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "../../../Users/Webest/node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./src/js/dropdownmenu.js":
/*!********************************!*\
  !*** ./src/js/dropdownmenu.js ***!
  \********************************/
/*! exports provided: DropDownMenu */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DropDownMenu", function() { return DropDownMenu; });
class DropDownMenu {
	constructor (container) {
		this.container = container;
	}

	init () {
		this.container.addEventListener('click', this._onClick.bind(this));
		this.container.addEventListener('mouseover', this._onMouseOver.bind(this));
		this.container.addEventListener('mouseleave', this._onMouseLeave.bind(this));
		return this;
	}

	_onMouseOver (e) {
		if (e.target.classList.contains('onhover')) {
			try {
				$(e.target)
					.closest('.dropdown')
					.children('.dropdown__list')
					.finish()
					.slideDown(200);
			} catch (e) {}
		}
	}

	_onClick (e) {
		var parent, menu;
		if (e.target.classList.contains('submenu')) {
			e.stopPropagation();
		}
		parent = $(e.target).closest('.dropdown').get(0);
		if (parent.dataset && parent.dataset.menu) {
			try {
				$('#' + parent.dataset.menu)
					.finish()
					.slideToggle(200);
			} catch (e) {}
		} else {
			$(parent)
				.children('.dropdown__list')
				.finish()
				.slideToggle(200);
		}
	}

	_onMouseLeave (e) {
		this.close.call(this);
	}

	close () {
		$(this.container)
			.children('.dropdown__list')
			.finish()
			.slideUp(200);
	}
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "../../../Users/Webest/node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./src/js/faqtabs.js":
/*!***************************!*\
  !*** ./src/js/faqtabs.js ***!
  \***************************/
/*! exports provided: FAQTabs */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FAQTabs", function() { return FAQTabs; });
/* harmony import */ var _accordion__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./accordion */ "./src/js/accordion.js");


class FAQTabs {
    constructor (container) {
        this.container = container;
        this.nav = this.container.querySelector('.faq__nav');
        this.panels = this.container.querySelectorAll('.faq__panel');
        this.accordion = this.container.querySelectorAll('.accordion');
        this.activeTab = this.panels.item(0);
    }

    init () {
        $(this.activeTab).show();
        this.accordion.forEach(a => {
            new _accordion__WEBPACK_IMPORTED_MODULE_0__["Accordion"] (a).init();
        });
        this.nav.addEventListener('click', this._onTabActivate.bind(this));
        return this;
    }

    _onTabActivate (e) {
        if (e.target.dataset.id) {
            $(e.target)
                .addClass('active')
                .siblings('li')
                .removeClass('active');
            const intendedToActivate = $(this.panels)
                .filter(function () {
                    return this.id === e.target.dataset.id;
                })
                .eq(0);
            $(this.activeTab).fadeOut(() => $(intendedToActivate).fadeIn());
            this.activeTab = intendedToActivate;
        }
    }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "../../../Users/Webest/node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./src/js/textinput.js":
/*!*****************************!*\
  !*** ./src/js/textinput.js ***!
  \*****************************/
/*! exports provided: TextInput */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TextInput", function() { return TextInput; });
class TextInput {

    constructor (container) {
        this.container = container;
        this.input = this.container.querySelector('input');
        this.placeholder = this.container.querySelector('.placeholder');
        this.errorString = this.container.querySelector('.error-string');
    }

    init () {
        this.input.addEventListener('focus', this._onFocus.bind(this));
        this.input.addEventListener('blur', this._onBlur.bind(this));
        return this;
    }

    _onBlur () {
        var error, addClass;
        error = '';
        addClass = this.input.value ? 'valid' : '';
        $(this.placeholder).toggleClass('shifted', this.input.value);
        if (!this.input.checkValidity()) {
            error = this.input.validationMessage;
            addClass = 'invalid';
        }
        $(this.container)
            .removeClass('invalid valid')
            .addClass(addClass)
        $(this.errorString).text(error);
    }

    _onFocus () {
        $(this.container).removeClass('invalid valid');
        $(this.placeholder).toggleClass('shifted', true);
    }

    reset () {
        $(this.container).removeClass('valid invalid');
    }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "../../../Users/Webest/node_modules/jquery/dist/jquery.js")))

/***/ })

/******/ });
//# sourceMappingURL=app.js.map